#include <stdio.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <string.h>

#define PORT 5000
#define MAXLINE 1000
#define TRUE 1
#define FALSE 0

char *readStringLine()
{
    char input[100];
    char *result = fgets(input, 100, stdin);
    if (result == NULL)
    {
        return "";
    }
    if (result[strlen(result) - 1] == '\n')
    {
        result[strlen(result) - 1] = 0;
    }
    return result;
}

int main()
{
    char buffer[100];
    int listenfd, len;
    struct sockaddr_in servaddr, cliaddr;
    bzero(&servaddr, sizeof(servaddr));

    // Create a UDP Socket
    listenfd = socket(AF_INET, SOCK_DGRAM, 0);
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servaddr.sin_port = htons(PORT);
    servaddr.sin_family = AF_INET;

    // bind server address to socket descriptor
    bind(listenfd, (struct sockaddr *)&servaddr, sizeof(servaddr));

    //receive the datagram
    while (TRUE)
    {
        len = sizeof(cliaddr);
        int n = recvfrom(listenfd, buffer, sizeof(buffer),
                         0, (struct sockaddr *)&cliaddr, &len);

        buffer[n] = '\0';
        puts(buffer);

        char *message = readStringLine();

        // send the response
        sendto(listenfd, message, MAXLINE, 0,
               (struct sockaddr *)&cliaddr, sizeof(cliaddr));
    }
}