#include <unistd.h> 
#include <stdio.h> 
#include <sys/socket.h> 
#include <stdlib.h> 
#include <netinet/in.h> 
#include <string.h> 

#define PORT 8080 
#define BUFFER_SIZE 1024
#define TRUE 1
#define FALSE 2

char* readStringLine(){
    char input[BUFFER_SIZE];
    char* result = fgets(input, BUFFER_SIZE, stdin);
    if (result == NULL) {
        return "";
    }
    if (result[strlen(result) - 1] == '\n') {
        result[strlen(result) - 1] = 0;
    }
    return result;
}

int main(int argc, char const *argv[]) 
{ 
    int server_fd;
    int new_socket;
    int readedMessageLength; 

    struct sockaddr_in address; 

    int opt = 1; 
    int addrlen = sizeof(address); 
    char buffer[BUFFER_SIZE] = {0}; 
    char *end = "bye";
       
    // Creating socket file descriptor 
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) 
    { 
        perror("socket failed"); 
        exit(EXIT_FAILURE); 
    } 
       
    // Forcefully attaching socket to the port 8080 
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, 
                                              &opt, sizeof(opt))) 
    { 
        perror("setsockopt"); 
        exit(EXIT_FAILURE); 
    } 

    address.sin_family = AF_INET; 
    address.sin_addr.s_addr = INADDR_ANY; 
    address.sin_port = htons( PORT ); 
       
    // Forcefully attaching socket to the port 8080 
    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0)                    
    { 
        perror("bind failed"); 
        exit(EXIT_FAILURE); 
    } 

    if (listen(server_fd, 3) < 0) 
    { 
        perror("listen"); 
        exit(EXIT_FAILURE); 
    } 
    
    if ((new_socket = accept(server_fd, (struct sockaddr *)&address,  
                       (socklen_t*)& addrlen))<0) 
    { 
        perror("accept"); 
        exit(EXIT_FAILURE); 
    } 

    printf("Server Start at PORT %d\n", PORT);

    while(TRUE) {
        readedMessageLength = read(new_socket , buffer, BUFFER_SIZE); 
        if(strcmp(buffer, end) == 0) {
           printf("Bye bye Server\n"); 
           close(new_socket);
           exit(EXIT_SUCCESS);
        }

        printf("Client> %s\n", buffer);

        printf("Server> ");
        char *message = readStringLine();
        send(new_socket , message , strlen(message) , 0); 
    }
    return EXIT_SUCCESS; 
} 