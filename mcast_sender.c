#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#define TRUE 1
#define FALSE 0
#define port 5000
#define group "239.255.255.250"

char *readStringLine()
{
    char input[100];
    char *result = fgets(input, 100, stdin);
    if (result == NULL)
    {
        return "";
    }
    if (result[strlen(result) - 1] == '\n')
    {
        result[strlen(result) - 1] = 0;
    }
    return result;
}

void exitWithError(const char *error)
{
    perror(error);
    exit(EXIT_FAILURE);
}

int main(int argc, char *argv[])
{
    const int delay_secs = 1;

    int socketfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (socketfd < 0)
    {
        exitWithError("socket");
    }

    struct sockaddr_in addr;
    bzero(&addr, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = inet_addr(group);
    addr.sin_port = htons(port);

    while (TRUE)
    {
        printf("Message -> ");
        char *message = readStringLine();
        char ch = 0;
        int nbytes = sendto(socketfd, message, strlen(message), 0, (struct sockaddr *)&addr, sizeof(addr));

        if (nbytes < 0)
        {
            exitWithError("sendto");
        }

        sleep(delay_secs);
    }

    return 0;
}