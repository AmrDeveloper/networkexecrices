#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <time.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#define TRUE 1
#define FALSE 0
#define port 5000
#define BUFFER_SIZE 256
#define group "239.255.255.250" //IP address classe D

void exitWithError(const char *error)
{
    perror(error);
    exit(EXIT_FAILURE);
}

int main(int argc, char *argv[])
{
    int socketfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (socketfd < 0)
    {
        exitWithError("socket");
    }

    u_int yes = 1;
    if (setsockopt(socketfd, SOL_SOCKET, SO_REUSEADDR, (char *)&yes, sizeof(yes)) < 0)
    {
        exitWithError("Reusing ADDR failed");
    }

    struct sockaddr_in addr;
    bzero(&addr, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = htonl(INADDR_ANY);
    addr.sin_port = htons(port);

    if (bind(socketfd, (struct sockaddr *)&addr, sizeof(addr)) < 0)
    {
        exitWithError("bind");
    }

    //Join the multicast group
    struct ip_mreq mreq;
    mreq.imr_multiaddr.s_addr = inet_addr(group);
    mreq.imr_interface.s_addr = htonl(INADDR_ANY);
    if (setsockopt(socketfd, IPPROTO_IP, IP_ADD_MEMBERSHIP, (char *)&mreq, sizeof(mreq)) < 0)
    {
        exitWithError("setsockopt");
    }

    while (TRUE)
    {
        char message[BUFFER_SIZE];
        int addrlen = sizeof(addr);
        int nbytes = recvfrom(socketfd, message, BUFFER_SIZE, 0, (struct sockaddr *)&addr, &addrlen);

        if (nbytes < 0)
        {
            exitWithError("recvfrom");
        }
        message[nbytes] = '\0';
        puts(message);
    }
    return EXIT_SUCCESS;
}
