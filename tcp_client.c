#include <sys/socket.h> 
#include <arpa/inet.h> 
#include <unistd.h> 
#include <stdlib.h>
#include <stdio.h> 
#include <string.h> 

#define PORT 8080 
#define BUFFER_SIZE 1024
#define TRUE 1
#define FALSE 2

char* readStringLine(){
    char input[BUFFER_SIZE];
    char* result = fgets(input, BUFFER_SIZE, stdin);
    if (result == NULL) {
        return "";
    }
    if (result[strlen(result) - 1] == '\n') {
        result[strlen(result) - 1] = 0;
    }
    return result;
}

int main(int argc, char const *argv[]) 
{ 
    int sock = 0;
    int messageLength; 
    struct sockaddr_in serv_addr; 
    char buffer[BUFFER_SIZE] = {0}; 
    char *end = "bye";

    // AF_INET : IPv4 protocol
    //SOCK_STREAM: TCP(reliable, connection oriented)
    //OCK_DGRAM: UDP(unreliable, connectionless)
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) 
    { 
        printf("\n Socket creation error \n"); 
        return EXIT_FAILURE; 
    } 
 
    serv_addr.sin_family = AF_INET; 
    serv_addr.sin_port = htons(PORT); 
       
    // Convert IPv4 and IPv6 addresses from text to binary form 
    if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0)  
    { 
        printf("\nInvalid address/ Address not supported \n"); 
        return EXIT_FAILURE; 
    } 
   
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) 
    { 
        printf("\nConnection Failed \n"); 
        return EXIT_FAILURE; 
    } 

    while(TRUE) {
        printf("Client> ");
        char *message = readStringLine();

        if(strcmp(message, end) == 0) {
           send(sock , message, strlen(message) , 0); 
           printf("Bye Bye client\n");
           close(sock);
           exit(EXIT_SUCCESS);
        }

        if(message == NULL) {
            continue;
        }
        
        send(sock , message, strlen(message) , 0); 
       
        messageLength = read( sock , buffer, BUFFER_SIZE);
        if(strcmp(buffer, end) == 0) {
           printf("Bye Bye client\n");
           close(sock);
           exit(EXIT_SUCCESS);
        }

        printf("Server> %s\n", buffer); 
    }
    return EXIT_SUCCESS; 
} 