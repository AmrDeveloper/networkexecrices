#include <stdio.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#define PORT 5000
#define MAXLINE 1000
#define TRUE 1
#define FALSE 0

char *readStringLine()
{
    char input[100];
    char *result = fgets(input, 100, stdin);
    if (result == NULL)
    {
        return "";
    }
    if (result[strlen(result) - 1] == '\n')
    {
        result[strlen(result) - 1] = 0;
    }
    return result;
}

int main()
{
    char buffer[100];
    char *terminate = "bye";
    int sockfd, n;
    struct sockaddr_in servaddr;

    // clear servaddr
    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_addr.s_addr = inet_addr("127.0.0.1");
    servaddr.sin_port = htons(PORT);
    servaddr.sin_family = AF_INET;

    // create datagram socket
    sockfd = socket(AF_INET, SOCK_DGRAM, 0);

    // connect to server
    if (connect(sockfd, (struct sockaddr *)&servaddr, sizeof(servaddr)) < 0)
    {
        printf("\n Error : Connect Failed \n");
        exit(0);
    }

    while (TRUE)
    {
        char *message = readStringLine();
        if (strcmp(message, terminate) == 0)
            break;

        sendto(sockfd, message, MAXLINE, 0, (struct sockaddr *)NULL, sizeof(servaddr));

        recvfrom(sockfd, buffer, sizeof(buffer), 0, (struct sockaddr *)NULL, NULL);
        puts(buffer);
    }

    // close the descriptor
    close(sockfd);
}